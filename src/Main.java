import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Encrypt encrypt = null;//класс для шифрования
        Decrypt decrypt = null;//класс для расшифровка

        Scanner scanner = new Scanner(System.in);
        int p = 0;
        int q = 0;
        System.out.println("Введите значение для P");
        p = scanner.nextInt();//принимеаем значения для p
        System.out.println("Введите значение для Q");
        q = scanner.nextInt();//принимаем значения для Q



        if (dividible(p) && dividible(q)) {//проверяем простыл ли числа
            encrypt = new Encrypt(p, q);
            decrypt = new Decrypt(p, q);
        } else {//если все неправильно то выходит из программы
            System.out.println("Вы ввели неправильные числа. Оба числа дожны быть простыми");
            System.exit(0);
        }

        System.out.println("Введите ваш текст");
        scanner.nextLine();
        String text = scanner.nextLine();//сохраняет текст
        System.out.println("Text = "+ text);


        System.out.println("Введите E если вам надо зашифровать текст");
        System.out.println("Введите D если вам надо дешифровать текст");
        String answer = scanner.next();//принимает ответ
        if (answer.equalsIgnoreCase("E")) {
            System.out.println("Идет зашифрование текста");
            encrypt.encrypt(text);//запускает метод, который шифрует
        } else if (answer.equalsIgnoreCase("D")) {
            System.out.println("Идет расшифрование текста");
            String[] textForDecrypt = decrypt.divide(text);//делит строковой тип на стрковой массив, так как с ним легче работать, когда здесь цифры
            String result = decrypt.decrypt(textForDecrypt);//запускает метод, который расшифрует весь текст
            System.out.println("Result="+result);
        } else {
            System.out.println("Вы ввели команду неправильно. Попробуйте запустить программу снова");//если введены неправильные данные, то выходит из программы
            System.exit(0);
        }


    }

    public static boolean dividible(int a) {
        boolean result = false;
        for (int i = 2; i < a; i++) {
            if (a % i == 0) {
                result = false;
                break;
            } else {
                result = true;
            }
        }
        return result;
    }
}
