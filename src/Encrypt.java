import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Aslan on 06.12.2016.
 */
public class Encrypt {
    private int P = 0;
    private int Q = 0;
    private int Fn = 0;
    private int exp = 1;
    private int N = 0;

    private static Map abc = new LinkedHashMap<String, Integer>();

    public Encrypt(int P, int Q) {
        String abcS = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        int n = 10;
        for (char c : abcS.toCharArray()) {//метод, который определяет алфавит
            abc.put(c, n);
            n += 1;
            abc.put(' ', 99);
        }

        Fn = (P - 1) * (Q - 1);//находит значение функции Эйлера по формуле для простых чисел
        System.out.println("Fn= " + Fn);

        N = P * Q;
        System.out.println("N=" + N);

        exp=expValue(Fn,exp);//для нахождения е записан отдельный метод
        System.out.println("e = "+exp);

    }

    public String encrypt(String text) {//метод, который шифрует данные
        System.out.println("In method encrypt");
        String result = "";
        text = text.toLowerCase();//полностью переопределяет на нижний регистр
        for (char c : text.toCharArray()) {
            if (c == ' ') {
                result += "99" + " ";//заменяет 99 на пробелы
                System.out.println(99);
            } else {
                result += getInt((Integer) abc.get(c)) + " ";//находи уже зашифрованный текст
            }
        }
        System.out.println("Result is = "+result);
        return result;
    }

    public int getInt(int B) {

        int kaldyk = (int) (Math.pow(B, exp - 1) % getN());
        kaldyk = (kaldyk * B) % getN();
        if (kaldyk < getN()) {
            return kaldyk;
        } else {
            return 0;
        }

    }

    public int expValue(int Fn, int e) {//метод который находит е
        int result = 3;
        for(int i=2; i<Fn;i++){
            if(divideble(i)&&i<Fn&&bothProstoe(Fn,i) ){//проверяет по всем условиям.
                System.out.println("Резултат найден и равен ="+i);
                result=i;
                break;//если результат найден, то выходит из цикла, так как надо найти самое минимальное

            }
        }
        return result;
    }

    public static boolean divideble(int a) {//метод, который проверяет простое ли число
        boolean result = false;
        for (int i = 2; i < a; i++) {
            if (a % i == 0) {
                result = false;
                break;
            } else {
                result = true;
            }
        }
        return result;
    }

    public  static boolean bothProstoe(int a, int b ){//проверяет взаимно простые ли числа
        boolean result = true;
        int max = (a>b)?(max= a):(max=b);
        for (int i=2;i<max;i++){
            if(a%i==0 && b%i==0){
                result=false;
                break;
            }
        }
        return result;
    }
    public int getP() {
        return P;
    }

    public int getQ() {
        return Q;
    }

    public int getFn() {
        return Fn;
    }

    public int getExp() {
        return exp;
    }

    public int getN() {
        return N;
    }

    public static Map getAbc() {
        return abc;
    }

    public static void setAbc(Map abc) {
        Encrypt.abc = abc;
    }

    public void setP(int p) {
        P = p;
    }

    public void setQ(int q) {
        Q = q;
    }

    public void setFn(int fn) {
        Fn = fn;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public void setN(int n) {
        N = n;
    }


}
