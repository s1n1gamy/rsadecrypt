import java.math.BigInteger;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Aslan on 06.12.2016.
 */
public class Decrypt {
    private int P = 0;
    private int Q = 0;
    private int Fn = 0;
    private int exp = 1;
    private int N = 0;

    private static Map abcReverse = new LinkedHashMap<String, Integer>();

    public Decrypt(int P, int Q) {//конструктор для класса дешифровки
        String abcS = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        int ind = 10;
        for (int i = 0; i < abcS.length(); i++) {
            abcReverse.put(ind, abcS.charAt(i));
            ind += 1;
        }
        Fn = (P-1)*(Q-1);
        System.out.println("Fn= "+Fn);

        N =P*Q;
        System.out.println("N="+N);

        exp=expValue(Fn,exp);
        System.out.println("e = "+exp);


    }

    public String decrypt(String text[]) {//метод, который занимается дешифровки
        String originalText = "";
        int E = 0;
        int D = 1;
        D = findD(D);
        System.out.println("D=" + D);
        long buff = 0;
        BigInteger buffBig ;//переводим число в формат BigInteger , так как с этим классом легче работать с большими степенями
        BigInteger nBig = BigInteger.valueOf(getN());

        BigInteger dBig = BigInteger.valueOf(D);
        for (int i = 0; i < text.length; i++) {//находит все по формуле
            E = Integer.parseInt(text[i]);//берет значение из строковго массива и преобразует его в целое число
            BigInteger eBig = BigInteger.valueOf(E);
            buffBig = eBig.pow(D) ;//находит степень числа E по степени D, который мы нашли в другом методе
            System.out.println(E + "  в степени " + D + "  = " + buffBig);

            buffBig=buffBig.remainder(nBig);//находит остаток
            System.out.println("Остаток при делении  = " + buffBig);

            if (E == 99) {//если число равно 99 , то сразу можно проброзовать его в пробел
                originalText += " ";
            } else {
                originalText += getAbcReverse().get(Integer.parseInt(String.valueOf(buffBig)));//находит остаток и дешифрует его в оригинальный текст
            }
        }
        return originalText;
    }

    public String[] divide(String text) {//метод, который используется для преобразования строковго тип на строкой массив
        String result = "";
        //text = text.replaceAll("99", " ");
        String[] parts = text.split(" ");
        //System.out.println("Длина равна = " + parts.length);

        return parts;
    }


    public int findD(int D) {//находим степень
        while ((getExp() * D) % getFn() != 1) {
            D += 1;
        }
        return D;
    }

    public int expValue(int Fn, int b) {//тот же метод, который нужен для нахждения е
        int result = 5;
        for(int i=2; i<Fn;i++){
            if(divideble(i)&&i<Fn&&bothProstoe(Fn,i) ){
                System.out.println("Резултат найден и равен ="+i);
                result=i;
                break;

            }
        }
        return result;
    }

    public static boolean divideble(int a) {
        boolean result = false;
        for (int i = 2; i < a; i++) {
            if (a % i == 0) {
                result = false;
                break;
            } else {
                result = true;
            }
        }
        return result;
    }

    public  static boolean bothProstoe(int a, int b ){
        boolean result = true;
        int max = (a>b)?(max= a):(max=b);
        for (int i=2;i<max;i++){
            if(a%i==0 && b%i==0){
                result=false;
                break;
            }
        }
        return result;
    }

    public int getP() {
        return P;
    }

    public int getQ() {
        return Q;
    }

    public int getFn() {
        return Fn;
    }

    public int getExp() {
        return exp;
    }

    public int getN() {
        return N;
    }

    public static Map getAbcReverse() {
        return abcReverse;
    }

    public static void setAbcReverse(Map abcReverse) {
        Decrypt.abcReverse = abcReverse;
    }

    public void setP(int p) {
        P = p;
    }

    public void setQ(int q) {
        Q = q;
    }

    public void setFn(int fn) {
        Fn = fn;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public void setN(int n) {
        N = n;
    }
}
